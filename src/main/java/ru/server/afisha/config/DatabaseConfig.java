package ru.server.afisha.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Configuration
@EnableJpaRepositories("ru.server.afisha.repository")
@EnableTransactionManagement//Транзакции
@PropertySource("classpath:db.properties")
@ComponentScan("ru.server.afisha")
public class DatabaseConfig {

	@Resource
	private Environment env;

	@Bean
	//создание таблиц ит д.
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean () {
		LocalContainerEntityManagerFactoryBean en = new LocalContainerEntityManagerFactoryBean();
		en.setDataSource(dataSource());
		en.setPackagesToScan(env.getRequiredProperty("db.entity.package"));
		en.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		en.setJpaProperties(getHibernateProperties());
		return en;
	}

	@Bean
	public PlatformTransactionManager transactionManagerBean() {
		JpaTransactionManager manager = new JpaTransactionManager();
		manager.setEntityManagerFactory(entityManagerFactoryBean().getObject());
		return manager;
	}

	private Properties getHibernateProperties() {
		try {
			Properties properties = new Properties();
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("hibernate.properties");
			properties.load(inputStream);
			return properties;
		} catch (IOException e) {
			throw new IllegalArgumentException("Can't find 'hibernate.properties' in classpath!", e);
		}
	}


	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl(env.getRequiredProperty("db.url"));
		dataSource.setDriverClassName(env.getRequiredProperty("db.driver"));
		dataSource.setUsername(env.getRequiredProperty("db.user"));
		dataSource.setPassword(env.getRequiredProperty("db.password"));

		dataSource.setInitialSize(Integer.valueOf(env.getRequiredProperty("db.initialSize")));
		dataSource.setMinIdle(Integer.valueOf(env.getRequiredProperty("db.minIdle")));
		dataSource.setMaxIdle(Integer.valueOf(env.getRequiredProperty("db.maxIdle")));
		dataSource.setTimeBetweenEvictionRunsMillis(Long.valueOf(env.getRequiredProperty("db.timeBetweenEvictionRunsMillis")));
		dataSource.setMinEvictableIdleTimeMillis(Long.valueOf(env.getRequiredProperty("db.minEvictableIdleTimeMillis")));
		dataSource.setTestOnBorrow(Boolean.valueOf(env.getRequiredProperty("db.testOnBorrow")));
		dataSource.setValidationQuery(env.getRequiredProperty("db.validationQuery"));

		return dataSource;
	}
}
