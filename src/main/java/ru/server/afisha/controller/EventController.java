package ru.server.afisha.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.server.afisha.entity.Event;
import ru.server.afisha.repository.EventRepository;

import java.util.List;

@RestController

public class EventController {

	private final EventRepository eventRepository;

	public EventController(EventRepository eventRepository) {
		this.eventRepository = eventRepository;
	}

	@RequestMapping(value = "/events", method = RequestMethod.GET)
	@ResponseBody
	public List<Event> getAllEvents() {
		return eventRepository.findAll();
	}



}
