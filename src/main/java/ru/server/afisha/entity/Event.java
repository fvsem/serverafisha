package ru.server.afisha.entity;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = ("afisha"))

public class Event {
	@Id
	@GeneratedValue(generator = "increment")
	@GenericGenerator(name = "increment",strategy = "increment")
	private long id;

	@Column(name = ("title"),nullable = false, length = 255)
	private String title;
	@Column (name = ("dateEvent"),nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataEvent;

	public Event() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDataEvent() {
		return dataEvent;
	}

	public void setDataEvent(Date dataEvent) {
		this.dataEvent = dataEvent;
	}
}
