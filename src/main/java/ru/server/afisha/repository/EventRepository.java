package ru.server.afisha.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.server.afisha.entity.Event;

public interface EventRepository extends JpaRepository<Event,Long> {
}
